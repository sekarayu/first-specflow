﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Bit8Pages
{
    public class Driver
    {
        public static IWebDriver Instance { get; set; }

        public static void Initialize()
        {
            string path = System.IO.Directory.GetCurrentDirectory();
            Instance = new ChromeDriver(path);
            waitAmoment();
            Instance.Manage().Window.Maximize();
        }

        public static string BaseAddress
        {
            get { return Utils.Url; }
        }

        public static void Navigate()
        {
            Instance.Navigate().GoToUrl(BaseAddress);
        }

        public static void Close()
        {
            Instance.Close();
        }

        private static void waitAmoment()
        {
            Instance.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }
    }
}
