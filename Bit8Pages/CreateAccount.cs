﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bit8Pages
{
    public class CreateAccount
    {
        public static void ChooseRegisterMenu()
        {
            var regmenu = Driver.Instance.FindElement(By.Id("register_menu"));
            regmenu.Click();
        }

        public static void FillName(string name)
        {
            Driver.Instance.FindElement(By.Id("user_name")).SendKeys(name);
        }

        public static void FillEmail(string email)
        {
            Driver.Instance.FindElement(By.Id("user_email")).SendKeys(email);
        }

        public static void FillPassword(string pass)
        {
            Driver.Instance.FindElement(By.Id("user_password")).SendKeys(pass);
        }

        public static void FillPasswordConfirmation(string passconfirm)
        {
            Driver.Instance.FindElement(By.Id("user_pass_confirm")).SendKeys(passconfirm);
        }

        public static void SubmitRegisterData()
        {
            var submitregister = Driver.Instance.FindElement(By.Id("submitbtn"));
            submitregister.Click();
        }

        public static void CheckUserLoggedInSuccess()
        {
            string actualmsg = Driver.Instance.FindElement(By.Id("alert_msg")).Text;
            Assert.IsTrue(actualmsg.Contains(Utils.loggedinmsg));
        }

        public static void AlertDisplay(string alertid)
        {
            switch (alertid)
            {
                case "blank name":
                    string blankname = Driver.Instance.FindElement(By.Id("name_msg")).Text;
                    Assert.AreEqual(Utils.blankname, blankname);
                    break;
                case "blank email":
                    string blankemail = Driver.Instance.FindElement(By.Id("email_msg")).Text;
                    Assert.AreEqual(Utils.blankemail, blankemail);
                    break;
                case "blank password":
                    string blankpass = Driver.Instance.FindElement(By.Id("pass_msg")).Text;
                    Assert.AreEqual(Utils.blankpass, blankpass);
                    break;
                case "name invalid":
                    string nameinvalid = Driver.Instance.FindElement(By.Id("name_msg")).Text;
                    Assert.AreEqual(Utils.nameinvalid, nameinvalid);
                    break;
                case "password mismatch":
                    string passmismatch = Driver.Instance.FindElement(By.Id("conf_pass_msg")).Text;
                    Assert.AreEqual(Utils.passmismatch, passmismatch);
                    break;
            }
        }
    }
}
