﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bit8Pages
{
    public class Utils
    {
        public static string Url = "http://localhost:8000";
        public static string loggedinmsg = "Your account has been created";
        public static string blankname = "The name field is required.";
        public static string blankemail = "The email field is required.";
        public static string blankpass = "The password field is required.";
        public static string nameinvalid = "The name must be at least 5 characters.";
        public static string passmismatch = "The password-confirm and password must match.";
    }
}
