﻿Feature: CreateAccount
	As a registration system
	I want to create a customer account

Scenario: Create Account
	Given the user open the page
	When the user choose register menu
	And the user input 'validuser@mail.com' email
	And the user input 'userpass' password
	And the user input 'valid user' name
	And the user input 'userpass' password confirmation
	And the user submit registration data
	Then the user will logged in successfully

Scenario: Create Account - Blank Name
	Given the user open the page
	When the user choose register menu
	And the user input 'blankname@mail.com' email
	And the user input 'userpass' password
	And the user input 'userpass' password confirmation
	And the user submit registration data
	Then the user will get 'blank name' alert

Scenario: Create Account - Blank Email
	Given the user open the page
	When the user choose register menu
	And the user input 'userpass' password
	And the user input 'blank email' name
	And the user input 'userpass' password confirmation
	And the user submit registration data
	Then the user will get 'blank email' alert

Scenario: Create Account - Blank Password
	Given the user open the page
	When the user choose register menu
	And the user input 'passwblank@mail.com' email
	And the user input 'pass blank' name
	And the user input 'userpass123' password confirmation
	And the user submit registration data
	Then the user will get 'blank password' alert

Scenario: Create Account - Invalid Name
	Given the user open the page
	When the user choose register menu
	And the user input 'invalidname@email.com' email
	And the user input 'userpass' password
	And the user input 'test' name
	And the user input 'userpass' password confirmation
	And the user submit registration data
	Then the user will get 'name invalid' alert

Scenario: Create Account - Password Mismatch
	Given the user open the page
	When the user choose register menu
	And the user input 'passwmismatch@mail.com' email
	And the user input 'userpass' password
	And the user input 'pass mismatch' name
	And the user input 'userpass123' password confirmation
	And the user submit registration data
	Then the user will get 'password mismatch' alert