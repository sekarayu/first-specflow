﻿using System;
using TechTalk.SpecFlow;
using Bit8Pages;

namespace Bit8Test
{
    [Binding]
    public class CreateAccountSteps
    {
        [BeforeScenario]
        public void SetUp()
        {
            Driver.Initialize();
            Driver.Instance.Navigate().GoToUrl(Driver.BaseAddress);
        }

        [AfterScenario]
        public void TearDown()
        {
            Driver.Close();
        }

        [Given(@"the user open the page")]
        public void GivenTheUserOpenThePage()
        {
            
        }
        
        [When(@"the user choose register menu")]
        public void WhenTheUserChooseRegisterMenu()
        {
            CreateAccount.ChooseRegisterMenu();
        }
        
        [When(@"the user input '(.*)' email")]
        public void WhenTheUserInputEmail(string p0)
        {
            CreateAccount.FillEmail(p0);
        }
        
        [When(@"the user input '(.*)' password")]
        public void WhenTheUserInputPassword(string p0)
        {
            CreateAccount.FillPassword(p0);
        }
        
        [When(@"the user input '(.*)' name")]
        public void WhenTheUserInputName(string p0)
        {
            CreateAccount.FillName(p0);
        }
        
        [When(@"the user input '(.*)' password confirmation")]
        public void WhenTheUserInputPasswordConfirmation(string p0)
        {
            CreateAccount.FillPasswordConfirmation(p0);
        }
        
        [When(@"the user submit registration data")]
        public void WhenTheUserSubmitRegistrationData()
        {
            CreateAccount.SubmitRegisterData();
        }
        
        [Then(@"the user will logged in successfully")]
        public void ThenTheUserWillLoggedInSuccessfully()
        {
            CreateAccount.CheckUserLoggedInSuccess();
        }
        
        [Then(@"the user will get '(.*)' alert")]
        public void ThenTheUserWillGetAlert(string p0)
        {
            CreateAccount.AlertDisplay(p0);
        }
    }
}
